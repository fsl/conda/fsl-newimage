#!/usr/bin/env bash

set -e

export FSLDIR=$PREFIX
export FSLDEVDIR=$PREFIX

. $FSLDIR/etc/fslconf/fsl-devel.sh

make
make install

make insertcopyright

mkdir -p $PREFIX/src/
cp -r $(pwd) $PREFIX/src/$PKG_NAME
